package fatec2018;

import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.awt.*;
import robocode.AdvancedRobot;


public class IronFist extends AdvancedRobot {

    int count = 0; // Controle de tempo
    
    double gunTurnAmt; // Quanto virar nossa arma ao procurar
    String trackName; // Nome do robô que estamos rastreando no momento
    String aliado1 = "fatec2018.LukeCage*";// Cria string para guardar o nome dos aliados
    String aliado2 = "fatec2018.Deradevil*";// Cria string para guardar o nome dos aliados
    String aliado3 = "samplesentry.BorderGuard";// Cria string para guardar o nome dos aliados

   
    public void run() {
        // Seta as cores do robo
        setScanColor(Color.yellow);
        setBulletColor(Color.black);
        setBodyColor(Color.black);
        setGunColor(Color.black);
        setRadarColor(Color.black);

        // Prepara a arma
        setAdjustGunForRobotTurn(true); // Mantenha a arma parada quando nos virarmos
        gunTurnAmt = 10; // Initialize gunTurn to 10

        // Loop do robo
        while (true) {
            // Gira a arma para procurar inimigo
            turnGunRight(360);
 
        }
    }

    /**
     * onScannedRobot: Scanneia os robos
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        
        String enemyName = e.getName(); //salva o nome do robo scaneado em uma string
        
        // Se o  alvo estiver longe, nosso robo segue em frente siga em frente.
        if (e.getDistance() > 150) {
   
            
            
            //verifica se o robo scaneado é aliado ou não.
            if (!(enemyName.equals(aliado1)) && !(enemyName.equals(aliado2)) && !(enemyName.equals(aliado3))) {
                
                gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading())); //normaliza o a mira

                turnGunRight(gunTurnAmt); // gira a arma para o inimigo
                turnRight(e.getBearing()); //Gira a carcaça para o angolo do inimigo
                setAhead(e.getDistance()); // Avança para cima do inimigo
                execute();
                
                //verifica o angulo da arma
                if (getGunHeading() == 0) {
                    fire(2);
                } else {
                    fire(1);
                }
            } else {
                turnGunRight(gunTurnAmt); //gira a arma 10º
            }

            return; //retorna
        }
        
        // Se o inimigo estiver próximo e não for nosso aliado
        if (!(enemyName.equals(aliado1)) && !(enemyName.equals(aliado2)) && !(enemyName.equals(aliado3))) {
            gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading())); //normaliza o a mira
            turnGunRight(gunTurnAmt); // gira a arma para o inimigo

            fireBullet(3); //Tiro fatal
        }

        scan();
    }

    /**
     * onHitRobot: Caso o robo colida com outro
     */
    public void onHitRobot(HitRobotEvent e) {
        

        String enemyName = e.getName();  //salva o nome do robo colidido em uma string
        
        //Verifica se não é aliado
        if (!(enemyName.equals(aliado1)) && !(enemyName.equals(aliado2)) && !(enemyName.equals(aliado3))) {
            
            gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading()));
            turnGunRight(gunTurnAmt);  //normaliza o a mira
            fire(3); //Tiro fatal
        }

        back(20); // volta 20
    }

    /**
     * onWin: Dancinah da vitoria
     */
    public void onWin(WinEvent e) {
        for (int i = 0; i < 50; i++) {
            turnRight(30);
            turnLeft(30);
        }
    }
}
