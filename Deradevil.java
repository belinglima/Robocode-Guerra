package fatec2018;

import java.awt.Color;
import robocode.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
/**
 * Deradevil - a robot by (Rodrigo Klaes)
 */
public class Deradevil extends AdvancedRobot {
    // Cria strings para guardar o nome dos aliados
    String aliado1 = "fatec2018.IronFist*";
    String aliado2 = "fatec2018.LukeCage*";
    String aliado3 = "samplesentry.BorderGuard";

    @Override
    public void run() {

        // Seta as cores
        setScanColor(Color.yellow);
        setBulletColor(Color.black);
        setBodyColor(Color.black);
        setGunColor(Color.black);
        setRadarColor(Color.black);

        // Lopp do Robô
        while (true) {
            ahead(50);
            turnGunRight(360);
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {

        // Cria uma variavel e guarda o nome do inimigo
        String enemyName = e.getName();

        //verifica se o inimigo é o Walls
        if (enemyName.equals("sample.Walls")) {

            //verifica a propia energia energia
            if (getEnergy() > 15) {

                //Verifica a distancia para fazer ajustes no angulo
                if (e.getDistance() < 300) {
                    double giraArma = normalRelativeAngleDegrees(e.getBearing() + 6 + getHeading() - getGunHeading()); //Normaliza a mira e ajusta angulo
                    turnGunRight(giraArma); //Gira a arma para o inimigo
                    fireBullet(2);
                } else if (e.getDistance() < 500) {
                    double giraArma = normalRelativeAngleDegrees(e.getBearing() + 17 + getHeading() - getGunHeading()); //Normaliza a mira e ajusta angulo
                    turnGunRight(giraArma); //Gira a arma para o inimigo
                    fireBullet(2);
                } else if (e.getVelocity() == 0) { // verifica se o robo esta parado
                    double giraArma = normalRelativeAngleDegrees(e.getBearing() + getHeading() - getGunHeading()); //Normaliza a mira
                    turnGunRight(giraArma); //Gira a arma para o inimigo
                    fireBullet(3); // tiro fatal
                }
            }
        } else {
            //verifica se a vida esta a cima de 15
            if (getEnergy() > 15) {
                
                //verifica se o robo detectado não é aliado
                if (!(enemyName.equals(aliado1)) && !(enemyName.equals(aliado2)) && !(enemyName.equals(aliado3))) {
                    double giraArma = normalRelativeAngleDegrees(e.getBearing() + getHeading() - getGunHeading()); //Normaliza a mira
                    turnGunRight(giraArma); //Gira a arma para o inimigo
                    
                    //verifica a distancia para dar o tiro
                    if (e.getDistance() < 500) {
                        fireBullet(1); //tiro rapidos
                    }
                }
            }
        }
        
        ahead(70); //avança 70
    }
    

    /**
     *  Caso colida na parede
     * @param e
     */
    @Override
    public void onHitWall(HitWallEvent e) {
        back(20); // volta 20
        turnRight(90); // gira 90º para a direita 
        ahead(20); // avança 20
    }

}
