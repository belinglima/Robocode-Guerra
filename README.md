## Engenheiros de software 

- Salomão Lima
- Felipe Picoli
- Marcos Camargo


## Engenharia de software.

Trabalho avaliativo em engenharia de software, aonde vamos demonstrar as estratégias,
de um robô em java, os programadores iram executar as estrategias aqui apresentadas. 



### Programadores

- Gabriel Santos
- Rodrigo Klaes
- Lucas Viegas
- Picoli


## Milestones & Issues

Os Engenheiros de Software definirão milestones com prazo de uma semana, sendo estas compostas por uma ou
mais issues, com peso definido para cada tarefa.

O prazo de solução das issues deve ser respeitado, ficando o Programador sujeito a
desligamento da equipe em caso de descumprimento não justificado. 

Os Engenheiros de Software reportarão semanalmente o andamento do projeto ao professor Angelo.

As Duvidas serão respondidas diretamente na milestone de duvida ou reunião da equipe.



## Documentação e Explicação de como funciona.


- [Documentação Oficial do Robocode](http://robocode.sourceforge.net)


- [Explicação sobre o Robô](explanation_of_robot.pdf)


Nos links acima vocês encontram o que precisam para começar a construir seu robô!
esperamos que vocês consigam entregar um robô de qualidade e que possamos vencer a batalha de robôs 




## Estrategias

- [As estrategias estão neste link](https://docs.google.com/document/d/1N9pHuS-StUnRgsEWd5pr6g8gy77lWYzLuqW-YL3RKVc/edit?usp=sharing)




#### Alguns Utilitarios para ajudar!

- [Scan](https://translate.google.com/translate?sl=en&tl=pt&js=y&prev=_t&hl=pt-BR&ie=UTF-8&u=http%3A%2F%2Fold.robowiki.net%2Frobowiki%3FRadar%2FOld&edit-text=&act=url)

- [Ajuda com o Robo](http://old.robowiki.net/robowiki?Strategy)

- [Mais Ajuda com o scanner](https://translate.google.com/translate?sl=en&tl=pt&js=y&prev=_t&hl=fr&ie=UTF-8&u=http%3A%2F%2Fmark.random-article.com%2Fweber%2Fjava%2Frobocode%2Flesson3.html&edit-text=&act=url)



#### Qualquer duvida favor enviar por aqui!

- [Perguntas - Duvidas](https://gitlab.com/belinglima/Robocode-Guerra/issues/4)




