package fatec2018;

import robocode.*;
import java.awt.Color;

public class LukeCage extends AdvancedRobot {

    public void run() {

        setBodyColor(Color.black);				// Sele��o da cor do chassis
        setGunColor(Color.black);				// Sele��o da cor da arma
        setRadarColor(Color.black);				// Sele��o da cor do radar
        setScanColor(Color.black);				// Sele��o da cor do scanner

        while (true) {
            double x = getX();					// Verifica a posi��o do rob� em X
            double y = getY();					// Verifica a posi��o do rob� em y
            double ang = 360 - getHeading();	// Posiciona o rob� no 0� apontando chassis do rob� para cima da arena 

            if ((x >= 100) && (x <= 900) && (y >= 100) && (y <= 900)) { // Faz a verifica��o se o rob� est� fora das bordas

                setAhead(100);					// Movimenta��o para frente
                setTurnLeft(90);				// Vira o rob� para a esquerda
                setTurnGunRight(360);			// Vira a arma do rob� para a direita

            } else if (x >= 500 && y >= 500) { // Movimenta��o para o rob� sair do canto do 1� quadrante e ir para o centro
                turnLeft(ang + 135);
                ahead(300);
            } else if (x <= 500 && y >= 500) { // Movimenta��o para o rob� sair do canto do 2� quadrante e ir para o centro
                turnRight(ang + 135);
                ahead(300);
            } else if (x <= 500 && y <= 500) { // Movimenta��o para o rob� sair do canto do 3� quadrante e ir para o centro
                turnRight(ang + 45);
                ahead(300);
            } else { 						   // Movimenta��o para o rob� sair do canto do 4� quadrante e ir para o centro
                turnLeft(ang + 45);
                ahead(300);
            }
            execute();							// Executa o loop
        }

    }

    public void onScannedRobot(ScannedRobotEvent inimigo) {

        String roboRadarTime = inimigo.getName();								// Pega o nome do rob� que passou no scanner

        String time1 = "fatec2018.Deredevil*";									// Declarado o pacote e o nome dos rob� do time
        String time2 = "fatec2018.IronFist*";									// Declarado o pacote e o nome dos rob� do time
        String time3 = "fatec2018.LukeCage*";									// Declarado o pacote e o nome dos rob� do time
        String time4 = "samplesentry.BorderGuard*";								// Declarado o pacote e o nome do BorderGuard

        double dist = inimigo.getDistance();									// Verifica a dist�ncia do rob� que passou pelo scanner
        String enemyName = inimigo.getName();
        
        if (!roboRadarTime.equals(time1) && !roboRadarTime.equals(time2)
                && !roboRadarTime.equals(time3) && !roboRadarTime.equals(time4)) {		// Cria uma exce��o para n�o atirar nos robos declarados 
            // nas Strings anteriormente
            
            if (dist <= 200) {													// Cria uma condi��o para atirar somente a menos de 200 px
                fire(3);
            } else if (dist >= 200 && dist < 400) {								// Cria uma condi��o para atirar somente entre 200 e 400 px
                fire(2);
            }
        }
    }

    public void onHitByBullet(HitByBulletEvent e) {								// Quando o rob� levar um tiro
        turnLeft(90);
        back(100);
    }

    public void onHitWall(HitWallEvent e) {										// Quando o rob� colidir com a parede
        back(200);
        turnLeft(90);
    }

    public void onHitRobot(HitRobotEvent e) {									// Quando o rob� colidir com outro rob�
        back(50);
        fire(3);
        turnLeft(90);
    }
}
